---
title: 在2020年捡一台iPhone4
tags: 数码
date: 2020-10-17
category: 数码
excerpt: 一代经典
index_img: https://i.loli.net/2020/12/26/wIYA3xEZtjOVpFL.jpg
banner_img: https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Apple_A4_Chip.jpg/1024px-Apple_A4_Chip.jpg
---

# 捡垃圾
下面这是我从同学手里捡来的写号机 <br/>

![战痕累累的iPhone4_背面与侧面](https://raw.githubusercontent.com/Sanksu/IMG/main/2022/iPhone4_1.jpg)
![战痕累累的iPhone4_背面与侧面_2](https://raw.githubusercontent.com/Sanksu/IMG/main/2022/iPhone4_3.jpg)
![战痕累累的iPhone4_正面](https://raw.githubusercontent.com/Sanksu/IMG/main/2022/iPhone4_4.jpg)



## 经典的锁屏界面

本来拿来想降到IOS6的，结果发现是iPhone3,3 orz<br/>

![网上找的](https://cdn.jsdelivr.net/gh/sanksu/Blog-Old@master/image/_posts/IOS6-unlock.jpg)


# 如今

IOS7已经没有多少能用的应用了 <br/>
旧版本的bilibili能打开，但不能登录账号，由于屏幕小，虽然是流畅分辨率，清晰的却还可以 <br/>
没有什么游戏可以玩。但是我还是找到了的安装包无尽之剑，即使现在被下架了，( ͡° ͜ʖ ͡°) <br/>

![贴吧找的](https://cdn.jsdelivr.net/gh/sanksu/Blog-Old@master/image/_posts/Infinity-Blade.jpg)
